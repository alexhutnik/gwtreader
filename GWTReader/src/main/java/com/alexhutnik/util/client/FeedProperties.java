package com.alexhutnik.util.client;

import com.alexhutnik.domain.Feed;
import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface FeedProperties extends PropertyAccess<Feed>{
	
	@Path("_id")
	ModelKeyProvider<Feed> key();
	
	ValueProvider<Feed,String> name();
	ValueProvider<Feed,String> url();
	
}
