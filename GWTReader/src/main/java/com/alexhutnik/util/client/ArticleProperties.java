package com.alexhutnik.util.client;

import com.alexhutnik.domain.Article;
import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

public interface ArticleProperties extends PropertyAccess<Article> {
	
	@Path("_id")
	ModelKeyProvider<Article> key();
	
	ValueProvider<Article,String> date();
	ValueProvider<Article,String> text();
}
