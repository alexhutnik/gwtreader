package com.alexhutnik.domain;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;

public class Feed_CustomFieldSerializer {
	public static void serialize(SerializationStreamWriter writer, Feed instance) throws SerializationException {

		writer.writeLong(instance.get_id());
		writer.writeString(instance.getName());
		writer.writeString(instance.getUrl());
		if (instance.getArticles() != null) {
			if (instance.getArticles().size() == 0) {
				writer.writeObject(new ArrayList<Article>());
			} else {
				writer.writeObject(new ArrayList<Article>(instance.getArticles()));
			}
		}
	}

	public static void deserialize(SerializationStreamReader reader, Feed instance) throws SerializationException {

		instance.set_id(reader.readLong());
		instance.setName(reader.readString());
		instance.setUrl(reader.readString());
	}
}
