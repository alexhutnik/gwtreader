package com.alexhutnik.domain;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;

public class User_CustomFieldSerializer {

	public static void serialize(SerializationStreamWriter writer, User instance) throws SerializationException {

		writer.writeLong(instance.get_id());
		writer.writeString(instance.getEmail());
		if (instance.getFeeds().size() == 0) {
			writer.writeObject(new ArrayList<Feed>());
		} else {
			writer.writeObject(new ArrayList<Feed>(instance.getFeeds()));
		}
	}

	public static void deserialize(SerializationStreamReader reader, User instance) throws SerializationException {

		instance.set_id(reader.readInt());
		instance.setEmail(reader.readString());
		instance.setFeeds((List<Feed>) reader.readObject());
	}

}
