package com.alexhutnik.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Article implements Serializable {
	
	@Id
	private long _id;
	private String text;
	private Date date;
	
	public Article(){
		//no-arg constructor needed for serializaion
	}
	
	public Article(String text, Date date){
		this.text = text;
		this.date = date;		
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
