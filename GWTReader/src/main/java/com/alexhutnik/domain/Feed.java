package com.alexhutnik.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Feed implements Serializable{
	
	private static final long serialVersionUID = 1677489431126499956L;
	
	@Id
	private long _id;
	private String url;
	private String name;
	
	@OneToMany
	@JoinColumn(name="_id")
	private List<Article> articles;
	
	@ManyToMany(mappedBy="feeds")
	private List<User> users;
	
	public Feed(){
		//no-arg contstructor needed for serialization
	}
	
	public Feed(String url){
		this.url = url;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Article> getArticles() {
		return articles;
	}
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public long get_id() {
		return _id;
	}

	public void set_id(long _id) {
		this._id = _id;
	}
}
