package com.alexhutnik.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alexhutnik.client.FeedService;
import com.alexhutnik.data.FeedRepository;
import com.alexhutnik.domain.Feed;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
@Service("feed")
public class FeedServiceImpl extends RemoteServiceServlet implements FeedService {

	@Autowired
	FeedRepository feedRepository;

	@Override
	@Transactional
	public List<Feed> getFeeds() throws IllegalArgumentException {
		Iterable<Feed> feeds = feedRepository.findAll();
		List<Feed> feedResult = new ArrayList<Feed>();
		if(feeds!=null){
			Iterator<Feed> i = feeds.iterator();
			while (i.hasNext()){
				Feed feed = i.next();
				feed.getArticles().size();
				feedResult.add(feed);
			}
		}

		return feedResult;
	}

	@Transactional
	public Feed addFeed(String url) throws IllegalArgumentException {
		return feedRepository.save(new Feed(url));
	}

}
