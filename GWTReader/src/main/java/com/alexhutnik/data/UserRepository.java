package com.alexhutnik.data;

import org.springframework.data.repository.CrudRepository;

import com.alexhutnik.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
