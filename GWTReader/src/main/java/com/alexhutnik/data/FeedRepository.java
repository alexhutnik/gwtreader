package com.alexhutnik.data;

import org.springframework.data.repository.CrudRepository;

import com.alexhutnik.domain.Feed;

public interface FeedRepository extends CrudRepository<Feed,Long>{
	
}
