package com.alexhutnik.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GWTReader implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";


	// Load UI labels
	private final Messages messages = GWT.create(Messages.class);
	
	private GridUiBinder gridUi; 

	public void onModuleLoad() {
		//TODO Set up UI
		gridUi = new GridUiBinder();
		RootPanel.get("center").add(gridUi.asWidget());
	}
}
