package com.alexhutnik.client;

import java.util.List;

import com.alexhutnik.domain.Feed;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("GWTServices/feed")
public interface FeedService extends RemoteService {
	public List<Feed> getFeeds();
	public Feed addFeed(String url);
}
