/**
 * Sencha GXT 3.0.1 - Sencha for GWT
 * Copyright(c) 2007-2012, Sencha, Inc.
 * licensing@sencha.com
 *
 * http://www.sencha.com/products/gxt/license/
 */
package com.alexhutnik.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.alexhutnik.domain.Feed;
import com.alexhutnik.util.client.FeedProperties;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.CellDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.CellDoubleClickEvent.CellDoubleClickHandler;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GridView;
import com.sencha.gxt.widget.core.client.info.Info;

public class GridUiBinder implements IsWidget {

	interface MyUiBinder extends UiBinder<Widget, GridUiBinder> {
	}

	private static MyUiBinder uiBinder = GWT.create(MyUiBinder.class);
	private static final FeedProperties feedProps = GWT.create(FeedProperties.class);

	private final FeedServiceAsync feedService = GWT.create(FeedService.class);

	private ColumnModel<Feed> cm;
	private ListStore<Feed> store;
	private Grid grid;

	@UiField
	GridView<Feed> gridView;

	@UiField
	TextButton addUrlButton;

	@UiField
	TextField addUrl;

	@Override
	public Widget asWidget() {

		ColumnConfig<Feed, String> urlCol = new ColumnConfig<Feed, String>(feedProps.url(), 200, "Url");

		List<ColumnConfig<Feed, ?>> l = new ArrayList<ColumnConfig<Feed, ?>>();
		l.add(urlCol);
		cm = new ColumnModel<Feed>(l);

		store = new ListStore<Feed>(feedProps.key());

		Widget component = uiBinder.createAndBindUi(this);

		// Have to call this after the component is built, otherwise NPE is
		// thrown
		gridView.setAutoExpandColumn(urlCol);

		return component;
	}

	@UiFactory
	Grid<Feed> createGrid() {
		grid = new Grid<Feed>(store, cm, gridView);
		
		//pre-populate the grid with whatever we've already got
		feedService.getFeeds(new AsyncCallback<List<Feed>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(List<Feed> result) {
				Iterator<Feed> i = result.iterator();
				while (i.hasNext()){
					store.add((Feed)i.next());
				}
				
			}
		});

		grid.addCellDoubleClickHandler(new CellDoubleClickHandler() {

			@Override
			public void onCellClick(CellDoubleClickEvent event) {
				Info.display("double click", store.get(event.getRowIndex()).getUrl());
				   Window w = new Window();
				    w.setHeadingText("Site for requested URL");
				    w.setModal(true);
				    w.setPixelSize(600, 400);
				    w.setMaximizable(true);
				    w.setWidget(new Frame(store.get(event.getRowIndex()).getUrl()));
				    w.show();

			}
		});

		return grid;
	}

	public ListStore<Feed> getStore() {
		return this.store;
	}

	@UiHandler({ "addUrlButton" })
	public void onSelect(SelectEvent event) {
		feedService.addFeed(addUrl.getCurrentValue(), new AsyncCallback<Feed>() {

			@Override
			public void onSuccess(Feed result) {
				// TODO Auto-generated method stub
				store.add(result);
				Info.display("Action", addUrl.getValue());
				addUrl.clear();

			}

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Info.display("ERROR", caught.toString());
			}
		});
	}
}